from bpy.props import *
from ...utility import *
from ...nodes.BASE.node_base import RenderNodeBase
from ...ui.icon_utils import RSN_Preview

from itertools import groupby

rsn_icon = RSN_Preview(image='RSN.bip', name='rsn_icon')


class ProcessorBarProperty(bpy.types.PropertyGroup):
    # store data into node's property
    task_list: StringProperty()
    cur_task: StringProperty()
    # draw properties
    done_col: FloatVectorProperty(name='Done Color', subtype='COLOR', default=(0, 1, 0), min=1, max=1)
    wait_col: FloatVectorProperty(name='Wait Color', subtype='COLOR', default=(0, 0, 0), min=1, max=1)


class TaskProperty(bpy.types.PropertyGroup):
    name: StringProperty(
        default="",
        name="Task Node Name",
        description="Name of the node")

    render: BoolProperty(name="Render", default=True, description="Use for render")


# use uilist for visualization
class RSN_UL_RenderTaskList(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        row = layout.row()
        nt = context.space_data.node_tree

        node = nt.nodes.get(item.name)
        if node:
            row.prop(node, 'is_active_task', text='', emboss=False,
                     icon="HIDE_OFF" if node.is_active_task else "HIDE_ON")
        else:
            row.label(text='', icon='ERROR')

        row.label(text=f'{item.name} | {node.label}')

        frame_count = (1 + node.frame_end - node.frame_start) // node.frame_step
        row.label(
            text=f'{node.frame_start}~{node.frame_end}({frame_count})')

        row.prop(item, "render", text="", icon="CHECKMARK")


class RSN_OT_UpdateTaskList(bpy.types.Operator):
    """Update List item"""
    bl_idname = "rsn.update_task_list"
    bl_label = "Update List"

    render_list_name: StringProperty()

    def execute(self, context):
        self.get_task_list()
        return {"FINISHED"}

    def get_task_list(self):
        tree = bpy.context.space_data.node_tree
        render_list_node = tree.nodes.get(self.render_list_name)

        task_list = render_list_node.get_dependant_tasks()
        remain = {}  # dict for the remain nodes

        for i, key in enumerate(render_list_node.task_list.keys()):
            if key not in task_list:
                render_list_node.task_list.remove(i)  # remove unlink nodes
                render_list_node.task_list_index -= 1 if render_list_node.task_list_index > 0 else 0
            else:
                # save render attribute
                item = render_list_node.task_list[i]
                attrs = {'render': item.render,
                         }
                remain[key] = attrs

        render_list_node.task_list.clear()  # clear list then add it back

        for name in task_list:
            item = render_list_node.task_list.add()
            item.name = name

            if name not in remain: continue

            attrs = remain[name]
            item.render = attrs['render']


def resize_node(self, context):
    if self.show_processor_bar:
        self.width *= 2
    else:
        self.width *= 0.5


class RSNodeRenderListNode(RenderNodeBase):
    """Render List Node"""
    bl_idname = 'RSNodeRenderListNode'
    bl_label = 'Render List'

    # action after render
    open_dir: BoolProperty(name='Open folder after render', default=True)
    clean_path: BoolProperty(name='Clean filepath after render', default=True)
    render_display_type: EnumProperty(items=[
        ('NONE', 'Keep User Interface', ''),
        ('SCREEN', 'Maximized Area', ''),
        ('AREA', 'Image Editor', ''),
        ('WINDOW', 'New Window', '')],
        default='WINDOW',
        name='Display')

    task_list: CollectionProperty(name="Task Property", type=TaskProperty)
    task_list_index: IntProperty(default=0, min=0)

    # processor
    show_processor_bar: BoolProperty(name='Processor Bar', update=resize_node)
    processor_bar: PointerProperty(name="Processor Property", type=ProcessorBarProperty)

    total_frames: IntProperty(default=0, min=0)
    done_frames: IntProperty(default=0, min=0)

    @classmethod
    def poll(cls, ntree):
        return ntree.bl_idname in {'RenderStackNodeTree'}

    def init(self, context):
        self.width = 200

    def get_dependant_tasks(self):
        node_list = []

        def append_node_to_list(node):
            """Skip the reroute node"""
            if len(node_list) == 0 or (len(node_list) != 0 and node.name != node_list[-1]):
                node_list.append(node.name)

        # @lru_cache(maxsize=None)
        def get_sub_node(node, pass_mute_node=True):
            for i, input in enumerate(node.inputs):
                if input.is_linked:
                    try:
                        sub_node = input.links[0].from_node
                        if sub_node.mute and pass_mute_node:
                            continue
                        else:
                            get_sub_node(sub_node)
                    except IndexError:  # This error shows when the dragging the link off a node(Works well with knife tool)
                        pass  # this seems to be a blender error
            # nodes append from left to right, from top to bottom
            if node.bl_idname in {'RenderNodeTask', 'RSNodeTaskNode'}: append_node_to_list(node)

        get_sub_node(self, True)

        return node_list

    def draw_buttons(self, context, layout):

        # left
        split = layout.split(factor=0.5 if self.show_processor_bar else 1)
        col = split.column()

        # col.operator('rsn.update_task_list').render_list_name = self.name

        col.template_list(
            "RSN_UL_RenderTaskList", "Task List",
            self, "task_list",
            self, "task_list_index", )

        # properties
        box = col.box().column(align=1)
        try:
            box = box.box()
            item = self.task_list[self.task_list_index]
            node = context.space_data.node_tree.nodes[item.name]

            box.label(text=f'{node.name} | {node.label}', icon='ALIGN_TOP')
            row = box.column(align=1)
            row.prop(node, 'path', text='')

            row = box.column(align=1)
            row.prop(node, 'frame_start', text="Frame Start")
            row.prop(node, 'frame_end')
            row.prop(node, 'frame_step')
        except IndexError:
            box.label(text='Connect at least one task node!')

        # bottom
        col.separator()
        row = col.row(align=1)
        row.scale_y = 1.25

        render = row.operator("rsn.render_queue", text=f'Render!',
                              icon='SHADING_RENDERED')  # icon_value=rsn_icon.get_image_icon_id()
        render.render_list_node_name = self.name

        save = row.operator("rsn.save_queue", text=f'Save',
                            icon='EXPORT')  # icon_value=rsn_icon.get_image_icon_id()

        save.render_list_node_name = self.name

        row.prop(self, 'show_processor_bar', icon='PREFERENCES', text='')

        # right
        # settings bar
        if self.show_processor_bar:
            col = split.column()

            sub_col = col.box().column(align=1)
            sub_col.label(text='Render Action', icon='SETTINGS')
            sub_col.prop(self, 'open_dir')
            sub_col.prop(self, 'clean_path')
            sub_col.prop(context.scene.render, "use_lock_interface", toggle=False)
            sub_col.prop(self, 'render_display_type')

            col.separator()

            sub_col = col.box().column(align=1)
            self.draw_processor_bar(context, sub_col)

    def update(self):
        self.auto_update_inputs('RSNodeSocketRenderList', "Task")
        bpy.ops.rsn.update_task_list(render_list_name=self.name)

    # TODO need to improve
    def draw_processor_bar(self, context, layout):
        bar = self.processor_bar
        task_list = bar.task_list.split(',')
        cur_id = task_list.index(bar.cur_task)

        total_frames = 0
        done_frames = 0

        col = layout.column(align=1)
        col.alignment = "CENTER"

        col = layout.column(align=1)
        # process of single task
        for index, item in enumerate(self.task_list):
            if not item.render: continue  # ignore the not render task

            node = context.space_data.node_tree.nodes[item.name]
            total_frames += node.frame_end - node.frame_start + 1

            task_name = node.name
            # finish list
            if index < cur_id:
                row = col.box().row(align=1)
                row.label(text=task_name, icon="CHECKBOX_HLT")

                finish = node.frame_end + 1 - node.frame_start
                row.label(text=f'100% {finish}/{finish}')
                done_frames += finish

            # current
            elif index == cur_id:
                if not context.window_manager.rsn_running_modal:
                    box = col.box()
                    row = box.row(align=1)

                    row.label(text=task_name, icon="CHECKBOX_HLT")
                    finish = node.frame_end + 1 - node.frame_start
                    row.label(text=f'100% {finish}/{finish}')

                    col.label(text='Render Finished!', icon='HEART')
                    done_frames += 1
                else:
                    row = col.box().row(align=1)
                    row.label(text=task_name, icon="RENDER_STILL")
                    done = context.scene.frame_current - node.frame_start
                    all = node.frame_end + 1 - node.frame_start
                    row.label(text=f"{round(done / all * 100, 2)}% {done}/{all}")

                    done_frames += done + 1
            # waiting
            elif index > cur_id:
                col.box().label(text=task_name, icon="CHECKBOX_DEHLT")

        total_process = round(done_frames / total_frames, 2)

        if context.window_manager.rsn_running_modal:
            col.label(text=f'Process: {total_process * 100}%', icon='SORTTIME')
            sub = col.split(factor=total_process, align=1)
            sub.scale_y = 0.25
            sub.prop(bar, "done_col", text='')
            sub.prop(bar, "wait_col", text='')


def register():
    rsn_icon.register()

    bpy.utils.register_class(ProcessorBarProperty)
    bpy.utils.register_class(TaskProperty)
    bpy.utils.register_class(RSN_OT_UpdateTaskList)
    bpy.utils.register_class(RSN_UL_RenderTaskList)
    bpy.utils.register_class(RSNodeRenderListNode)


def unregister():
    rsn_icon.unregister()

    bpy.utils.unregister_class(ProcessorBarProperty)
    bpy.utils.unregister_class(TaskProperty)
    bpy.utils.unregister_class(RSN_UL_RenderTaskList)
    bpy.utils.unregister_class(RSN_OT_UpdateTaskList)
    bpy.utils.unregister_class(RSNodeRenderListNode)
